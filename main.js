import { TeapotGeometry } from '/three.js/examples/jsm/geometries/TeapotGeometry.js';

var camera, scene, renderer;
var newCamera;
var cameraHelper;
var controls;
var parameters;
var jar;
const teapotSize = 2;
var directionalLight;
var object;
var object2;
var material;
var geometryInSolid;
var style;
var plane;
var flag = false;
var xro, yro, zro;

init();
update(renderer, scene, camera, controls);

function init() {
    scene = new THREE.Scene();    

    object = getBox(1,1,1);
    object.name = 'objectName';
    
    plane = getPlane(20);
    var ball = getLightBall(0.1);
    plane.name = 'plane-1';

    object.position.y = object.geometry.parameters.height/2;
    plane.rotation.x = Math.PI/2;
    
    // CAMERA
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.set(-15, 6, 5);
    camera.lookAt(new THREE.Vector3(0, 0, 0));

    // CAMERA HELPER
    newCamera = new THREE.PerspectiveCamera(30, window.innerWidth / window.innerHeight, 2, 40);
    newCamera.position.set(0, 2, 10);
    newCamera.lookAt(new THREE.Vector3(0, 0, 0));
    cameraHelper = new THREE.CameraHelper(newCamera);


    // LIGHT
    directionalLight = getDirectionalLight(1);
    directionalLight.position.x = 5;
    directionalLight.position.y = 4;
    directionalLight.position.z = 0;
    directionalLight.intensity = 2;

    // RENDERER
    renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.shadowMap.enabled = true;
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor('rgb(120, 120, 120)');
    document.getElementById('webgl').appendChild(renderer.domElement);

    // CONTROLS
    controls = new THREE.OrbitControls(camera, renderer.domElement);

    // SCENE
    scene.add(plane);
    // directionalLight.add(ball);
    scene.add(directionalLight);

    //Background scene
    // const loader = new THREE.TextureLoader();
    // const bgTexture = loader.load('./images/background.jpeg');
    // scene.background = bgTexture;
    setupGUI();  
}

function getLightBall(size) {
    var geometry = new THREE.SphereGeometry(size, 100, 30);
    var material = new THREE.MeshBasicMaterial({
        color: 'rgb(255, 255, 255)'
    });
    var mesh = new THREE.Mesh(geometry, material);
    return mesh;
}

function getPlane(size) {
    var geometry = new THREE.PlaneGeometry(size, size);
    var material = new THREE.MeshPhongMaterial({
        color: 'rgb(30, 30, 50)',
        side: THREE.DoubleSide
    });
    var mesh = new THREE.Mesh(geometry, material);
    mesh.receiveShadow = true;
    return mesh;
}

function getBox(w, h, d) {
    var geometry = new THREE.BoxGeometry(w, h, d);    
    var material = new THREE.MeshPhongMaterial({
        color: 'rgb(255, 255, 255)',
        // wireframe: true,
    });
    var mesh = new THREE.Mesh(geometry, material);
    mesh.castShadow = true;
    return mesh;
}

function getSphere(size) {
    var geometry = new THREE.SphereGeometry(size, 100, 30);
    var material = new THREE.MeshPhongMaterial({
        color: 'rgb(255, 255, 255)'
    });
    var mesh = new THREE.Mesh(geometry, material);
    mesh.castShadow = true;
    return mesh;
}

function getCone(radius, height, radialSegments) {
    var geometry = new THREE.ConeGeometry(radius, height, radialSegments);
    var material = new THREE.MeshPhongMaterial({
        color: 'rgb(255, 255, 255)'
    });
    var mesh = new THREE.Mesh(geometry, material);
    mesh.castShadow = true;
    return mesh;
}

function getCylinder(radiusTop, radiusBottom, height, radialSegments) {
    var geometry = new THREE.CylinderGeometry(radiusTop, radiusBottom, height, radialSegments);
    var material = new THREE.MeshPhongMaterial({
        color: 'rgb(255, 255, 255)'
    });
    var mesh = new THREE.Mesh(geometry, material);
    mesh.castShadow = true;
    return mesh;
}

function getTorus(radius, tube, radialSegments, tubularSegments) {
    var geometry = new THREE.TorusGeometry(radius, tube, radialSegments, tubularSegments);
    var material = new THREE.MeshPhongMaterial({
        color: 'rgb(255, 255, 255)'
    });
    var mesh = new THREE.Mesh(geometry, material);
    mesh.castShadow = true;
    return mesh;
}

function createNewTeapot() {
    const teapotGeometry = new TeapotGeometry(teapotSize);
    var material = new THREE.MeshPhongMaterial({
        color: 'rgb(255, 255, 255)'
    });
    var mesh = new THREE.Mesh(teapotGeometry, material);
    mesh.castShadow = true;
    return mesh;
}

function getPointLight(intensity) {
    var light = new THREE.PointLight(0xffffff, intensity);
    light.castShadow = true;

    return light;
}

function getSpotLight(intensity) {
    var light = new THREE.SpotLight(0xffffff, intensity);
    light.castShadow = true;

    light.shadow.bias = 0.001;
    light.shadow.mapSize.width = 2048;
    light.shadow.mapSize.height = 2048;

    return light;
}

function getDirectionalLight(intensity) {
    var light = new THREE.DirectionalLight(0xffffff, intensity);
    light.castShadow = true;
    light.shadow.bias = 0.001;

    light.shadow.camera.left = -10;
    light.shadow.camera.bottom = -10;
    light.shadow.camera.right = 10;
    light.shadow.camera.top = 10;
    return light;
}

function getAmbientLight(intensity) {
    var light = new THREE.AmbientLight('rgb(10, 30, 50)', intensity);
    return light;
}

function spin(varname, x, y, z) {
    var speed = 0.1;
    if (varname) {
        if (x) {
            object.rotation.x += speed;
        }
        else if (y) {
            object.rotation.y += speed;
        }
        else {
            object.rotation.z += speed;
        }
    }
}

function update(renderer, scene, camera, controls) {
    renderer.render(scene, camera);

    spin(xro, true, false, false);
    spin(yro, false, true, false);
    spin(zro, false, false, true);

    controls.update();

    requestAnimationFrame(function() {
        update(renderer, scene, camera, controls);
    })
}

function convertToPoints() {
    material = new THREE.PointsMaterial({
        color: object.material.color,
        sizeAttenuation: true,
        size: 0.1
    });
    object2 = new THREE.Points(object.geometry, material);

    var position = new THREE.Vector3();
    var quaternion = new THREE.Quaternion();
    var scale = new THREE.Vector3();

    object.matrixWorld.decompose(position, quaternion, scale);

    object2.position.set(position.x, position.y, position.z);
    object2.scale.set(scale.x, scale.y, scale.z);
    object2.rotation.set(quaternion.x, quaternion.y, quaternion.z);
    
    object2.name = 'objectName';
    object2.castShadow = false;
    object2.receiveShadow = true;

    object.geometry.dispose();
    object.material.dispose();

    scene.remove(object);
    object = object2;
    scene.add(object);
}

function convertToLines() {
    var wireframe = new THREE.WireframeGeometry(object.geometry);
    var line = new THREE.LineSegments(wireframe);
    line.material.depthTest = true;
    line.material.opacity = 1;
    line.material.transparent = true;
    line.material.color = object.material.color;

    object2 = line;

    var position = new THREE.Vector3();
    var quaternion = new THREE.Quaternion();
    var scale = new THREE.Vector3();

    object.matrixWorld.decompose(position, quaternion, scale);

    object2.position.set(position.x, position.y, position.z);
    object2.scale.set(scale.x, scale.y, scale.z);
    object2.rotation.set(quaternion.x, quaternion.y, quaternion.z);
    
    object2.name = 'objectName';
    object2.castShadow = true;
    object2.receiveShadow = true;

    object.geometry.dispose();
    object.material.dispose();

    scene.remove(object);
    object = object2;
    scene.add(object);
}

function convertToSolid() {
    material = new THREE.MeshPhongMaterial({
        color: object.material.color
    });
    object2 = new THREE.Mesh(geometryInSolid, material);

    var position = new THREE.Vector3();
    var quaternion = new THREE.Quaternion();
    var scale = new THREE.Vector3();

    object.matrixWorld.decompose(position, quaternion, scale);

    object2.position.set(position.x, position.y, position.z);
    object2.scale.set(scale.x, scale.y, scale.z);
    object2.rotation.set(quaternion.x, quaternion.y, quaternion.z);
    
    object2.name = 'objectName';
    object2.castShadow = true;
    object2.receiveShadow = false;

    object.geometry.dispose();
    object.material.dispose();

    scene.remove(object);
    object = object2;
    scene.add(object);
}

function addTexture() {
    var textureMap = new THREE.TextureLoader().load('images/earth.jpeg');
    textureMap.wrapS = textureMap.wrapT = THREE.RepeatWrapping;
    textureMap.anisotropy = 16;
    textureMap.encoding = THREE.sRGBEncoding;

    // MATERIALS
    var materialColor = new THREE.Color();
    materialColor.setRGB( 1.0, 1.0, 1.0 );
    var texturedMaterial = new THREE.MeshPhongMaterial({ 
        color: materialColor, 
        map: textureMap,
        side: THREE.DoubleSide 
    });
    
    object2 = new THREE.Mesh(geometryInSolid, texturedMaterial);
    var position = new THREE.Vector3();
    var quaternion = new THREE.Quaternion();
    var scale = new THREE.Vector3();

    object.matrixWorld.decompose(position, quaternion, scale);

    object2.position.set(position.x, position.y, position.z);
    object2.scale.set(scale.x, scale.y, scale.z);
    object2.rotation.set(quaternion.x, quaternion.y, quaternion.z);
    
    object2.name = 'objectName';
    object2.castShadow = true;
    object2.receiveShadow = false;

    object.geometry.dispose();
    object.material.dispose();

    scene.remove(object);
    object = object2;
    scene.add(object);
}

function setupGUI() {
    var gui = new dat.GUI();

    parameters = {
        geo: "", // geometry
        sty: "", // style (points/lines/solid)
        pla: true, // plane
        col: "#0000ff", // color
        pp: false, // perspective projection (Thực hiện chiếu phối cảnh, tăng giảm các toạ độ x,y,z near, far)
        aff: "", // affine
        lig: "", // light (totality, light source, shadow)
        tex: false, // texture

        // animation 
        x_ani: false, 
        y_ani: false,
        z_ani: false
    }

    var creation = gui.addFolder('Creation');
    var geometry = creation.add(parameters, 'geo', ["Box", "Sphere", "Cone", "Cylinder", "Torus", "Teapot"]).name('Geometry');
    style = creation.add(parameters, 'sty', ["Points", "Lines", "Solid"]).name('Style');
    creation.add(parameters, 'pla').name('Show plane').onChange(function() {
        if (parameters.pla==false) {scene.remove(plane);} 
        else {scene.add(plane);}
    });
    geometry.onChange(newGeometry => {
        texture.setValue(false); // uncheck texture box when choose another geometry
        x_animation.setValue(false); // uncheck animation box when choose another geometry
        y_animation.setValue(false); // uncheck animation box when choose another geometry
        z_animation.setValue(false); // uncheck animation box when choose another geometry
        scene.remove(object);
        flag = true;
        switch (newGeometry) {
            case "Box":
                object = getBox(3, 3, 3);
                geometryInSolid = object.geometry;
                style.setValue("Solid");
                object.position.y = object.geometry.parameters.height/2;
                break;
        
            case "Sphere":
                object = getSphere(2);
                geometryInSolid = object.geometry;
                style.setValue("Solid");
                object.position.y = object.geometry.parameters.radius;
                break;

            case "Cone":
                object = getCone(2, 3, 100);
                geometryInSolid = object.geometry;
                style.setValue("Solid");
                object.position.y = object.geometry.parameters.height/2;
                break;

            case "Cylinder":
                object = getCylinder(2, 2, 4, 100);
                geometryInSolid = object.geometry;
                style.setValue("Solid");
                object.position.y = object.geometry.parameters.height/2;
                break;

            case "Torus":
                object = getTorus(2, 1, 20, 100);
                geometryInSolid = object.geometry;
                style.setValue("Solid");
                object.position.y = object.geometry.parameters.radius*1.5;
                break;

            case "Teapot":
                object = createNewTeapot();	
                geometryInSolid = object.geometry;
                style.setValue("Solid");
                object.position.y = 2;		
                break;

            default:
                break;
        }
        scene.add(object);
    });
    creation.open();

    style.onChange(newStyle => {
        if (flag) { 
            switch (newStyle) {
                case "Points":
                    convertToPoints();
                    break;

                case "Lines":
                    convertToLines();
                    break;

                case "Solid":
                    convertToSolid();
                    break;

                default:
                    break;
            }
        }
    });

    // GUI chiếu phối cảnh
    var perspective_projection = gui.add(parameters, 'pp').name('Perspective Projection').onChange(function() {
        if (parameters.pp) {
            scene.add(cameraHelper);
        }
        else {
            scene.remove(cameraHelper);
        }
    });

    // GUI colors
    var color = gui.addColor(parameters, 'col').name('Color');
    color.onChange(function(jar) {
        object.material.color.setHex(jar.replace("#", "0x"));
    });

    // perspective_projection.add(spotLight, 'penumbra', 0, 1); // directionalLight don't have 'penumbra' parameter

    // gui affine
    var affine = gui.addFolder('Affine');
    var translation = affine.addFolder('Translation');
    translation.add(object.position, 'x', -20, 20).onChange(function(jar) {object.position.x = jar});
    translation.add(object.position, 'y', -20, 20).onChange(function(jar) {object.position.y = jar});
    translation.add(object.position, 'z', -20, 20).onChange(function(jar) {object.position.z = jar});
    translation.open();
    var rotation = affine.addFolder('Rotation');
    rotation.add(object.rotation, 'x', -20, 20).onChange(function(jar) {object.rotation.x = jar});
    rotation.add(object.rotation, 'y', -20, 20).onChange(function(jar) {object.rotation.y = jar});
    rotation.add(object.rotation, 'z', -20, 20).onChange(function(jar) {object.rotation.z = jar});
    rotation.open();
    var scaling = affine.addFolder('Scaling');
    scaling.add(object.scale, 'x', 0, 20).onChange(function(jar) {object.scale.x = jar});
    scaling.add(object.scale, 'y', 0, 20).onChange(function(jar) {object.scale.y = jar});
    scaling.add(object.scale, 'z', 0, 20).onChange(function(jar) {object.scale.z = jar});
    scaling.open();
    affine.open();

    // GUI light
    var light = gui.addFolder('Light');    
    light.add(directionalLight, 'intensity', 0, 10);
    light.add(directionalLight.position, 'x', -10, 10);
    light.add(directionalLight.position, 'y', -10, 10);
    light.add(directionalLight.position, 'z', -10, 10);
    light.open();

    // GUI texture
    var texture = gui.add(parameters, 'tex').name('Texture').onChange(function() {
        if (parameters.tex) {addTexture();}
        else {convertToSolid();}
    });

    // GUI animation
    
    var animation = gui.addFolder('Animation');
    var x_animation = animation.add(parameters, 'x_ani').name('Spin by X');
    var y_animation = animation.add(parameters, 'y_ani').name('Spin by Y');
    var z_animation = animation.add(parameters, 'z_ani').name('Spin by Z');
    x_animation.onChange(function(jar) {xro = jar;});
    y_animation.onChange(function(jar) {yro = jar;});
    z_animation.onChange(function(jar) {zro = jar;});
    animation.open();
}