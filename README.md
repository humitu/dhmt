# 3D models with threejs (Course Project)

## threejs
========
#### JavaScript 3D library ####

The aim of the project is to create an easy to use, lightweight, cross-browser, general purpose 3D library. The current builds only include a WebGL renderer but WebGPU (experimental), SVG and CSS3D renderers are also available in the examples.

[Examples](https://threejs.org/examples/) &mdash;
[Documentation](https://threejs.org/docs/) &mdash;
[Wiki](https://github.com/mrdoob/three.js/wiki) &mdash;
[Migrating](https://github.com/mrdoob/three.js/wiki/Migration-Guide) &mdash;
[Questions](http://stackoverflow.com/questions/tagged/three.js) &mdash;
[Forum](https://discourse.threejs.org/) &mdash;
[Slack](https://join.slack.com/t/threejs/shared_invite/zt-rnuegz5e-FQpc6YboDVW~5idlp7GfDw) &mdash;
[Discord](https://discordapp.com/invite/HF4UdyF)
## Requirements

- Nodejs v14 or newer

## Initialization

Clone this repo:

```
git clone https://bitbucket.org/humitu/dhmt.git
```

Navigate to workspace

```
cd dhmt
```

Install packages:

```
npm install --global http-server
```

Start:

```
http-server ./ -p 8000
```